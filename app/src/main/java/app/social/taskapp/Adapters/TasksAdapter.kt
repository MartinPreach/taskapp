package app.social.taskapp.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import app.social.taskapp.R
import app.social.taskapp.DAO.Task
import kotlinx.android.synthetic.main.item_task.view.*

class TasksAdapter (private val mContext: Context, private val listaTasks: List<Task>) : ArrayAdapter<Task>(mContext, 0, listaTasks) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.item_task, parent, false)

        val task = listaTasks[position]

        layout.titleTv.text = task.title
        layout.descriptionTv.text = task.description
        layout.date1Tv.text = task.date1
        layout.date2Tv.text = task.date2

        return layout
    }
}