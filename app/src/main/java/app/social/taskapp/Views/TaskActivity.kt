package app.social.taskapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import app.social.taskapp.Adapters.TasksAdapter
import app.social.taskapp.DAO.AppDatabase
import app.social.taskapp.R
import app.social.taskapp.DAO.Task
import kotlinx.android.synthetic.main.activity_task.*

class TaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        var listaTasks = emptyList<Task>()

        val database = AppDatabase.getDatabase(this)

        database.tasks().getAll().observe(this, Observer {
            listaTasks = it

            val adapter = TasksAdapter(this, listaTasks)

            tasksLv.adapter = adapter
        })

        tasksLv.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, DetailTaskActivity::class.java)
            intent.putExtra("id", listaTasks[position].idTask)
            startActivity(intent)
        }

        floatingActionButton.setOnClickListener {
            val intent = Intent(this, NewTaskActivity::class.java)
            startActivity(intent)
        }
    }
}
