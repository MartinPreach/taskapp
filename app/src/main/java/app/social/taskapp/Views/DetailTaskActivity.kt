package app.social.taskapp.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import app.social.taskapp.DAO.AppDatabase
import app.social.taskapp.R
import app.social.taskapp.DAO.Task
import kotlinx.android.synthetic.main.activity_detail_task.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailTaskActivity : AppCompatActivity() {

    private lateinit var database: AppDatabase
    private lateinit var task: Task
    private lateinit var taskLiveData: LiveData<Task>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_task)

        database = AppDatabase.getDatabase(this)

        val idTask = intent.getIntExtra("id", 0)

        taskLiveData = database.tasks().get(idTask)

        taskLiveData.observe(this, Observer {
            task = it

            titleTv.text = task.title
            descriptionTv.text = task.description
            date1Tv.text = task.date1
            date2Tv.text = task.date2
        })

        editTaskBtn.setOnClickListener {
            val intent = Intent(this, NewTaskActivity::class.java)
            intent.putExtra("task", task)
            startActivity(intent)
        }

        deleteTaskBtn.setOnClickListener {
            taskLiveData.removeObservers(this)

            CoroutineScope(Dispatchers.IO).launch {
                database.tasks().delete(task)
                this@DetailTaskActivity.finish()
            }
        }

    }
}
