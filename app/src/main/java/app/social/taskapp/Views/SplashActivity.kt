package app.social.taskapp.Views

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import app.social.taskapp.R

class SplashActivity : AppCompatActivity() {

    private lateinit var topAnimation: Animation
    private lateinit var bottomAnimation: Animation

    private lateinit var splash_image: ImageView
    private lateinit var app_name_txt: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        topAnimation = AnimationUtils.loadAnimation(this, R.anim.top)
        bottomAnimation = AnimationUtils.loadAnimation(this, R.anim.bottom)

        splash_image = findViewById(R.id.splash_image)
        app_name_txt = findViewById(R.id.app_name_txt)

        splash_image.animation = topAnimation
        app_name_txt.animation = bottomAnimation

        scheduleSplashScreen(this)
    }
    fun scheduleSplashScreen(context: Context){
        val splashDuration = getSplashScreenDuration()
        Handler().postDelayed({
            moveOn(context)
            finish()
        }, splashDuration)

    }


    private fun moveOn(context: Context){
        val intent = Intent(context, TaskActivity::class.java)
        context.startActivity(intent)
    }

    private fun getSplashScreenDuration() = 4000L
}
