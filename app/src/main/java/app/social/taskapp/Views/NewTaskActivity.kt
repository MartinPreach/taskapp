package app.social.taskapp.Views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.social.taskapp.DAO.AppDatabase
import app.social.taskapp.R
import app.social.taskapp.DAO.Task
import kotlinx.android.synthetic.main.activity_new_task.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewTaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_task)

        var idTask: Int? = null

        if (intent.hasExtra("task")) {
            val task = intent.extras?.getSerializable("task") as Task

            titleEt.setText(task.title)
            descriptionEt.setText(task.description)
            date1Et.setText(task.date1)
            date2Et.setText(task.date2)
            idTask = task.idTask
        }

        val database = AppDatabase.getDatabase(this)

        SaveBtn.setOnClickListener {
            val title = titleEt.text.toString()
            val description = descriptionEt.text.toString()
            val date1 = date1Et.text.toString()
            val date2 = date2Et.text.toString()

            val task = Task(title, description, date1, date2)

            if (idTask != null) {
                CoroutineScope(Dispatchers.IO).launch {
                    task.idTask = idTask

                    database.tasks().update(task)

                    this@NewTaskActivity.finish()
                }
            } else {
                CoroutineScope(Dispatchers.IO).launch {
                    database.tasks().insertAll(task)

                    this@NewTaskActivity.finish()
                }
            }
        }
    }
}
