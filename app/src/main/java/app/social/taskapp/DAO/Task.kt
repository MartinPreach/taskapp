package app.social.taskapp.DAO

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "tasks")
class Task (
    val title:String,
    val description: String,
    val date1: String,
    val date2: String,
    @PrimaryKey(autoGenerate = true)
    var idTask: Int = 0
    ) : Serializable