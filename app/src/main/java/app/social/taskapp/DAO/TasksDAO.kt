package app.social.taskapp.DAO

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TasksDAO {
    @Query("SELECT * FROM tasks")
    fun getAll(): LiveData<List<Task>>

    @Query("SELECT * FROM tasks WHERE idTask = :id")
    fun get(id: Int): LiveData<Task>

    @Insert
    fun insertAll(vararg tasks: Task)

    @Update
    fun update(task: Task)

    @Delete
    fun delete(task: Task)
}